/** @jsx jsx */
/** @jsxFrag  Fragment */
import { Hono, Context } from 'hono';
import { env } from 'hono/adapter';
import { html } from 'hono/html';
import { JSXRouter } from '../../src/preset/trie';
import { jsx } from 'hono/jsx';

const jsxRouter: JSXRouter<any> = new JSXRouter({
    config: {
        layout: {
            props: {
                env: (c: Context) => env<{NAME: string}>(c)['NAME'],
                func: (c: Context) => `foo-bar-baz`,
                runtime: (c: Context) => c.runtime
            }
        },
        page: {
            props: {
                text: 'This appears on all pages.'
            }
        }
    }
});

const app = new Hono({ router: jsxRouter});

jsxRouter.applyRoutes();

app.get('/env', (c) => {
    const { NAME } = env<{ NAME: string }>(c);
    return c.text(NAME);
});
app.get('/helloWorld', (c: Context) => {
    return c.text('Hello world');
});
app.get('/html', (c: Context) => {
    return c.html(html`<p>Hello world</p>`);
});
app.get('/inlineJSX', (c: Context) => {
    return c.html(<p>Hello world</p>);
});

export default app