import { html } from 'hono/html';
export default (props: { children?: any, func?: string, env?: string, runtime?: string }) => {
    return html`<!DOCTYPE html>
<html lang="EN-US">
    <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta name="theme-color" media="(prefers-color-scheme: light)" content="lightgray" />
        <meta name="theme-color" media="(prefers-color-scheme: dark)" content="gray" />
        <meta name="x-jsx-func" content="${props.func}" />
        <meta name="x-jsx-env" content="TS_JEST: ${props.env}" />
        <meta name="x-jsx-c" content="RUNTIME: ${props.runtime}" />
        <title>Example Site</title>
        <link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml" />
    </head>
    <body>${props.children}</body>
</html>`;
};