import Header from '../../components/Header';
// import { DocumentHead } from 'hono-jsx-router';

// export const head: DocumentHead = () => {
//     return {
//         title: 'Copy | About',
//         meta: [
            
//         ]
//     }
// }

export default (props: {text?: string}) => {
    return (<>
        <Header />
        <h1>Copy</h1>
        <p>Hello world!</p>
        <p>{props.text}</p>
    </>)
};