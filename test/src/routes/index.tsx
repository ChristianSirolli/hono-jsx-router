import Header from '../components/Header';
export default (props: {text?: string}) => {
    return (<>
        <Header />
        <h1>Home</h1>
        <p>Hello world!</p>
        <p>{props.text}</p>
    </>)
};