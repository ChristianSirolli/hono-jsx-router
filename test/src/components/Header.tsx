export default (props: { children?: any }) => {
    return (
        <header>
            <a href="/">Home</a>
            <a href="/about">About</a>
            <a href="/blog">Blog</a>
        </header>
    )
}