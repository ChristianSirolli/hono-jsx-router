/** @jsx jsx */
/** @jsxFrag  Fragment */
import { Context, Hono } from 'hono';
import { html } from 'hono/html';
import { JSXRouter } from '../src/main';
import { JSXRouter as LinearJSXRouter } from '../src/preset/linear';
import { JSXRouter as PatternJSXRouter } from '../src/preset/pattern';
import { JSXRouter as RegExpJSXRouter } from '../src/preset/reg-exp';
import { JSXRouter as SmartJSXRouter } from '../src/preset/smart';
import { JSXRouter as TrieJSXRouter } from '../src/preset/trie';
import { RegExpRouter } from 'hono/router/reg-exp-router';
import { TrieRouter } from 'hono/router/trie-router';
import { jsx } from 'hono/jsx';
import { env } from 'hono/adapter';
import { unstable_dev } from 'wrangler';
import type { UnstableDevWorker } from 'wrangler';
// import {describe, expect, beforeEach, it} from '@jest/globals';
import { jest } from '@jest/globals';
import { H } from 'hono/dist/types/types';
globalThis.Promise = jest.requireActual('promise');
// import { Promise } from 'promise'
function honoRoutes(app: Hono) {
    app.get('/helloWorld', (c: Context) => {
        return c.text('Hello world');
    });
    app.get('/html', (c: Context) => {
        return c.html(html`<p>Hello world</p>`);
    });
    app.get('/inlineJSX', (c: Context) => {
        return c.html(<p>Hello world</p>);
    });
    app.onError((err: Error, c) => {
        console.error(`Error in JSXRouter test: ${err.stack}`);
        return c.html(`<h1>HTTP 500</h1>`, 500);
    });
}
function nodeTests(app: Hono) {
    it('Should render HTML strings from JSX files', async () => {
        const res = await app.request('http://localhost/');
        expect(res.status).toBe(200);
        expect(res.headers.get('Content-Type')).toBe('text/html; charset=UTF-8');
        expect(await res.text()).toBe(`<!DOCTYPE html>
<html lang="EN-US">
    <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta name="theme-color" media="(prefers-color-scheme: light)" content="lightgray" />
        <meta name="theme-color" media="(prefers-color-scheme: dark)" content="gray" />
        <meta name="x-jsx-func" content="foo-bar-baz" />
        <meta name="x-jsx-env" content="TS_JEST: 1" />
        <meta name="x-jsx-c" content="RUNTIME: node" />
        <title>Example Site</title>
        <link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml" />
    </head>
    <body><header><a href="/">Home</a><a href="/about">About</a><a href="/blog">Blog</a></header><h1>Home</h1><p>Hello world!</p><p>This appears on all pages.</p></body>
</html>`);
    });
    it('Should return text', async () => {
        const res = await app.request('http://localhost/helloWorld');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Hello world');
    });
    it('Should return html', async () => {
        const res = await app.request('http://localhost/html');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should return html from JSX', async () => {
        const res = await app.request('http://localhost/inlineJSX');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should load wildcard routes', async () => {
        const res = await app.request('http://localhost/about');
        expect(res.status).toBe(200);
        expect(res.headers.get('Content-Type')).toBe('text/html; charset=UTF-8');
        expect(await res.text()).toBe(`<!DOCTYPE html>
<html lang="EN-US">
    <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta name="theme-color" media="(prefers-color-scheme: light)" content="lightgray" />
        <meta name="theme-color" media="(prefers-color-scheme: dark)" content="gray" />
        <meta name="x-jsx-func" content="foo-bar-baz" />
        <meta name="x-jsx-env" content="TS_JEST: 1" />
        <meta name="x-jsx-c" content="RUNTIME: node" />
        <title>Example Site</title>
        <link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml" />
    </head>
    <body><header><a href="/">Home</a><a href="/about">About</a><a href="/blog">Blog</a></header><h1>Home</h1><p>Hello world!</p><p>This appears on all pages.</p></body>
</html>`);
    });
    it('Should load wildcard routes (2)', async () => {
        const res = await app.request('http://localhost/about/test');
        expect(res.status).toBe(200);
        expect(res.headers.get('Content-Type')).toBe('text/html; charset=UTF-8');
        expect(await res.text()).toBe(`<!DOCTYPE html>
<html lang="EN-US">
    <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta name="theme-color" media="(prefers-color-scheme: light)" content="lightgray" />
        <meta name="theme-color" media="(prefers-color-scheme: dark)" content="gray" />
        <meta name="x-jsx-func" content="foo-bar-baz" />
        <meta name="x-jsx-env" content="TS_JEST: 1" />
        <meta name="x-jsx-c" content="RUNTIME: node" />
        <title>Example Site</title>
        <link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml" />
    </head>
    <body><header><a href="/">Home</a><a href="/about">About</a><a href="/blog">Blog</a></header><h1>Home</h1><p>Hello world!</p><p>This appears on all pages.</p></body>
</html>`);
    });
    it('Should load route next to wildcard', async () => {
        const res = await app.request('http://localhost/about/copy');
        expect(res.status).toBe(200);
        expect(res.headers.get('Content-Type')).toBe('text/html; charset=UTF-8');
        expect(await res.text()).toBe(`<!DOCTYPE html>
<html lang="EN-US">
    <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta name="theme-color" media="(prefers-color-scheme: light)" content="lightgray" />
        <meta name="theme-color" media="(prefers-color-scheme: dark)" content="gray" />
        <meta name="x-jsx-func" content="foo-bar-baz" />
        <meta name="x-jsx-env" content="TS_JEST: 1" />
        <meta name="x-jsx-c" content="RUNTIME: node" />
        <title>Example Site</title>
        <link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml" />
    </head>
    <body><header><a href="/">Home</a><a href="/about">About</a><a href="/blog">Blog</a></header><h1>Copy</h1><p>Hello world!</p><p>This appears on all pages.</p></body>
</html>`);
    });
}

describe('JSXRouter Basic Usage', () => {
    let router: JSXRouter<H> = new JSXRouter<H>({
        config: {
            layout: {
                props: {
                    env: (c: Context) => env<{ TS_JEST: string }>(c)['TS_JEST'],
                    func: (c: Context) => `foo-bar-baz`,
                    runtime: (c: Context) => c.runtime
                }
            },
            page: {
                props: {
                    text: 'This appears on all pages.'
                }
            }
        }
    });
    let app: Hono = new Hono({ router });
    router.applyRoutes();
    honoRoutes(app);
    // app.showRoutes()
    console.log(router.routes);
    nodeTests(app);
});

describe('JSXRouter with LinearRouter Usage', () => {
    let router: LinearJSXRouter<H> = new LinearJSXRouter<H>({
        config: {
            layout: {
                props: {
                    env: (c: Context) => env<{ TS_JEST: string }>(c)['TS_JEST'],
                    func: (c: Context) => `foo-bar-baz`,
                    runtime: (c: Context) => c.runtime
                }
            },
            page: {
                props: {
                    text: 'This appears on all pages.'
                }
            }
        }
    });
    let app: Hono = new Hono({ router });
    router.applyRoutes();
    honoRoutes(app);
    nodeTests(app);
});
describe('JSXRouter with PatternRouter Usage', () => {
    let router: PatternJSXRouter<H> = new PatternJSXRouter<H>({
        config: {
            layout: {
                props: {
                    env: (c: Context) => env<{ TS_JEST: string }>(c)['TS_JEST'],
                    func: (c: Context) => `foo-bar-baz`,
                    runtime: (c: Context) => c.runtime
                }
            },
            page: {
                props: {
                    text: 'This appears on all pages.'
                }
            }
        }
    });
    let app: Hono = new Hono({ router });
    router.applyRoutes();
    honoRoutes(app);
    nodeTests(app);
});
describe('JSXRouter with RegExpRouter Usage', () => {
    let router: RegExpJSXRouter<H> = new RegExpJSXRouter<H>({
        config: {
            layout: {
                props: {
                    env: (c: Context) => env<{ TS_JEST: string }>(c)['TS_JEST'],
                    func: (c: Context) => `foo-bar-baz`,
                    runtime: (c: Context) => c.runtime
                }
            },
            page: {
                props: {
                    text: 'This appears on all pages.'
                }
            }
        }
    });
    let app: Hono = new Hono({ router });
    router.applyRoutes();
    honoRoutes(app);
    nodeTests(app);
});
describe('JSXRouter with SmartRouter Usage', () => {
    let router: SmartJSXRouter<H> = new SmartJSXRouter<H>({
        config: {
            layout: {
                props: {
                    env: (c: Context) => env<{ TS_JEST: string }>(c)['TS_JEST'],
                    func: (c: Context) => `foo-bar-baz`,
                    runtime: (c: Context) => c.runtime
                }
            },
            page: {
                props: {
                    text: 'This appears on all pages.'
                }
            }
        },
        routers: [
            new RegExpRouter(),
            new TrieRouter()
        ]
    });
    let app: Hono = new Hono({ router });
    router.applyRoutes();
    honoRoutes(app);
    nodeTests(app);
});
describe('JSXRouter with TrieRouter Usage', () => {
    let router: TrieJSXRouter<H> = new TrieJSXRouter<H>({
        config: {
            layout: {
                props: {
                    env: (c: Context) => env<{ TS_JEST: string }>(c)['TS_JEST'],
                    func: (c: Context) => `foo-bar-baz`,
                    runtime: (c: Context) => c.runtime
                }
            },
            page: {
                props: {
                    text: 'This appears on all pages.'
                }
            }
        }
    });
    let app: Hono = new Hono({ router });
    router.applyRoutes();
    honoRoutes(app);
    nodeTests(app);
});