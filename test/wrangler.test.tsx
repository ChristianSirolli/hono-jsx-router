/** @jsx jsx */
/** @jsxFrag  Fragment */
import { Context, Hono } from 'hono';
import { html } from 'hono/html';
import { jsx } from 'hono/jsx';
import { unstable_dev } from 'wrangler';
import type { UnstableDevWorker } from 'wrangler';
// import {describe, expect, it, beforeAll, afterAll} from '@jest/globals';

const expectedIndex: string = `<!DOCTYPE html>
<html lang="EN-US">
    <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta name="theme-color" media="(prefers-color-scheme: light)" content="lightgray" />
        <meta name="theme-color" media="(prefers-color-scheme: dark)" content="gray" />
        <meta name="x-jsx-func" content="foo-bar-baz" />
        <meta name="x-jsx-env" content="TS_JEST: Cloudflare" />
        <meta name="x-jsx-c" content="RUNTIME: workerd" />
        <title>Example Site</title>
        <link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml" />
    </head>
    <body><header><a href="/">Home</a><a href="/about">About</a><a href="/blog">Blog</a></header><h1>Home</h1><p>Hello world!</p><p>This appears on all pages.</p></body>
</html>`;

describe('Wrangler Basic Usage', () => {
    let worker: UnstableDevWorker;
    beforeAll(async () => {
        worker = await unstable_dev('./test/src/index.tsx', {
            vars: {
                NAME: 'Cloudflare',
            },
            experimental: { disableExperimentalWarning: true },
        });
    });
    afterAll(async () => {
        await worker.stop();
    });
    it('Should return Hello World', async () => {
        const res = await worker.fetch('/');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe(expectedIndex);
    });
    it('Should return the environment variable', async () => {
        const res = await worker.fetch('/env');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Cloudflare');
    });
    it('Should return text', async () => {
        const res = await worker.fetch('http://localhost/helloWorld');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Hello world');
    });
    it('Should return html', async () => {
        const res = await worker.fetch('http://localhost/html');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should return html from JSX', async () => {
        const res = await worker.fetch('http://localhost/inlineJSX');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
});
describe('Wrangler LinearRouter Usage', () => {
    let worker: UnstableDevWorker;
    beforeAll(async () => {
        worker = await unstable_dev('./test/src/linear.tsx', {
            vars: {
                NAME: 'Cloudflare',
            },
            experimental: { disableExperimentalWarning: true },
        });
    });
    afterAll(async () => {
        await worker.stop();
    });
    it('Should return Hello World', async () => {
        const res = await worker.fetch('/');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe(expectedIndex);
    });
    it('Should return the environment variable', async () => {
        const res = await worker.fetch('/env');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Cloudflare');
    });
    it('Should return text', async () => {
        const res = await worker.fetch('http://localhost/helloWorld');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Hello world');
    });
    it('Should return html', async () => {
        const res = await worker.fetch('http://localhost/html');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should return html from JSX', async () => {
        const res = await worker.fetch('http://localhost/inlineJSX');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
});
describe('Wrangler PatternRouter Usage', () => {
    let worker: UnstableDevWorker;
    beforeAll(async () => {
        worker = await unstable_dev('./test/src/pattern.tsx', {
            vars: {
                NAME: 'Cloudflare',
            },
            experimental: { disableExperimentalWarning: true },
        });
    });
    afterAll(async () => {
        await worker.stop();
    });
    it('Should return Hello World', async () => {
        const res = await worker.fetch('/');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe(expectedIndex);
    });
    it('Should return the environment variable', async () => {
        const res = await worker.fetch('/env');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Cloudflare');
    });
    it('Should return text', async () => {
        const res = await worker.fetch('http://localhost/helloWorld');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Hello world');
    });
    it('Should return html', async () => {
        const res = await worker.fetch('http://localhost/html');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should return html from JSX', async () => {
        const res = await worker.fetch('http://localhost/inlineJSX');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
});
describe('Wrangler RegExpRouter Usage', () => {
    let worker: UnstableDevWorker;
    beforeAll(async () => {
        worker = await unstable_dev('./test/src/reg-exp.tsx', {
            vars: {
                NAME: 'Cloudflare',
            },
            experimental: { disableExperimentalWarning: true },
        });
    });
    afterAll(async () => {
        await worker.stop();
    });
    it('Should return Hello World', async () => {
        const res = await worker.fetch('/');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe(expectedIndex);
    });
    it('Should return the environment variable', async () => {
        const res = await worker.fetch('/env');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Cloudflare');
    });
    it('Should return text', async () => {
        const res = await worker.fetch('http://localhost/helloWorld');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Hello world');
    });
    it('Should return html', async () => {
        const res = await worker.fetch('http://localhost/html');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should return html from JSX', async () => {
        const res = await worker.fetch('http://localhost/inlineJSX');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should load wildcard routes', async () => {
        const res = await worker.fetch('http://localhost/about/');
        expect(res.status).toBe(200);
    });
    it('Should load wildcard routes (2)', async () => {
        const res = await worker.fetch('http://localhost/about/test');
        expect(res.status).toBe(200);
    });
});
describe('Wrangler SmartRouter Usage', () => {
    let worker: UnstableDevWorker;
    beforeAll(async () => {
        worker = await unstable_dev('./test/src/smart.tsx', {
            vars: {
                NAME: 'Cloudflare',
            },
            experimental: { disableExperimentalWarning: true },
        });
    });
    afterAll(async () => {
        await worker.stop();
    });
    it('Should return Hello World', async () => {
        const res = await worker.fetch('/');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe(expectedIndex);
    });
    it('Should return the environment variable', async () => {
        const res = await worker.fetch('/env');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Cloudflare');
    });
    it('Should return text', async () => {
        const res = await worker.fetch('http://localhost/helloWorld');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Hello world');
    });
    it('Should return html', async () => {
        const res = await worker.fetch('http://localhost/html');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should return html from JSX', async () => {
        const res = await worker.fetch('http://localhost/inlineJSX');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
});
describe('Wrangler TrieRouter Usage', () => {
    let worker: UnstableDevWorker;
    beforeAll(async () => {
        worker = await unstable_dev('./test/src/trie.tsx', {
            vars: {
                NAME: 'Cloudflare',
            },
            experimental: { disableExperimentalWarning: true },
        });
    });
    afterAll(async () => {
        await worker.stop();
    });
    it('Should return Hello World', async () => {
        const res = await worker.fetch('/');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe(expectedIndex);
    });
    it('Should return the environment variable', async () => {
        const res = await worker.fetch('/env');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Cloudflare');
    });
    it('Should return text', async () => {
        const res = await worker.fetch('http://localhost/helloWorld');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('Hello world');
    });
    it('Should return html', async () => {
        const res = await worker.fetch('http://localhost/html');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
    it('Should return html from JSX', async () => {
        const res = await worker.fetch('http://localhost/inlineJSX');
        expect(res.status).toBe(200);
        expect(await res.text()).toBe('<p>Hello world</p>');
    });
});