#!/bin/bash
SOURCE=$(readlink -f ${BASH_SOURCE[0]})
HONO_JSX_ROUTER_PATH="${PWD}/node_modules/hono-jsx-router"
ROUTES=$(cat "./package.json" | "${HONO_JSX_ROUTER_PATH}/bin/jq/jq-linux64" -r '.jsxRouter.path')
if [[ $ROUTES == "null" ]] || [[ $ROUTES == "" ]]; then
    echo "hono-jsx-router has not been configured."
    echo "Please add to your package.json:"
    echo "  {"
    echo "      ..."
    echo "     \"jsxRouter\": {"
    echo "       \"path\": \"./path/to/jsx/files\""
    echo "     }"
    echo "   }"
    exit 1
else
    IMPORT_PATH="${PWD}/${ROUTES}"
    pushd $ROUTES
    FILES=$(find . -name "*.tsx" -printf "'%P'\n")
    COMBINE_FILE="${HONO_JSX_ROUTER_PATH}/src/.combine.tsx"
    touch $COMBINE_FILE
    # echo "export const rootPath: string = '${IMPORT_PATH}';" > $COMBINE_FILE
    echo "export const combinedRoutes: Record<string,any> = {" > $COMBINE_FILE
    for FILE in $FILES; do
        echo Found $FILE
        echo "    ${FILE%.tsx\'}': await import('${IMPORT_PATH}/'+${FILE%.tsx\'}').then(page => page.default).catch(e => console.error(e))," >> $COMBINE_FILE
    done
    echo "};" >> $COMBINE_FILE
    popd
    exit 0
fi