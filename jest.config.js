/** @type {import('ts-jest').JestConfigWithTsJest} */
export default {
  extensionsToTreatAsEsm: ['.tsx', '.ts'],
  roots: [
    "./test"
  ],
  testMatch: ['**\.test\.tsx'],
  transform: {
    '^.+\\.tsx?$': [
      'ts-jest',
      {
        tsconfig: './tsconfig.json',
        useESM: true
      }
    ],
  },
  testPathIgnorePatterns: [
    "/node_modules/"
  ],
  // testEnvironment: 'miniflare',
  testEnvironmentOptions: {
    /*
    bindings: {
      __STATIC_CONTENT: {
        get: (key) => {
          const table = { 'index.abcdef.index': 'This is index' }
          return table[key]
        },
      },
      __STATIC_CONTENT_MANIFEST: JSON.stringify({
        'index.index': 'index.abcdef.index',
      }),
    },
    kvNamespaces: ['TEST_NAMESPACE'],
    */
  }
};