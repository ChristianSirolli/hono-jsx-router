import { SmartRouter } from 'hono/router/smart-router';
import { jsxRouterConstructor, applyRoutes } from '../router';

export class JSXRouter<T> extends SmartRouter<T> {
    name: string = 'JSXRouter with SmartRouter'
    config: Record<string, any> = {}
    constructor(init: Pick<JSXRouter<T>, 'config' | 'routers'>) {
        super(init);
        jsxRouterConstructor.call(this, init);
    }
    applyRoutes(this: any) {
        return applyRoutes.call(this);
    }
}