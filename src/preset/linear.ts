import { LinearRouter } from 'hono/router/linear-router';
import { jsxRouterConstructor, applyRoutes } from '../router';

export class JSXRouter<T> extends LinearRouter<T> {
    name: string = 'JSXRouter with LinearRouter'
    config: Record<string, any> = {}
    constructor(init: Pick<JSXRouter<T>, 'config'>) {
        super();
        jsxRouterConstructor.call(this, init);
    }
    applyRoutes(this: any) {
        return applyRoutes.call(this);
    }
}