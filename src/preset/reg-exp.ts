import { RegExpRouter } from 'hono/router/reg-exp-router';
import { jsxRouterConstructor, applyRoutes } from '../router';

export class JSXRouter<T> extends RegExpRouter<T> {
    name: string = 'JSXRouter with RegExpRouter'
    config: Record<string, any> = {}
    constructor(init: Pick<JSXRouter<T>, 'config'>) {
        super();
        jsxRouterConstructor.call(this, init);
    }
    applyRoutes(this: any) {
        return applyRoutes.call(this);
    }
}