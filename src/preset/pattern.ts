import { PatternRouter } from 'hono/router/pattern-router';
import { jsxRouterConstructor, applyRoutes } from '../router';

export class JSXRouter<T> extends PatternRouter<T> {
    name: string = 'JSXRouter with PatternRouter'
    config: Record<string, any> = {}
    constructor(init: Pick<JSXRouter<T>, 'config'>) {
        super();
        jsxRouterConstructor.call(this, init);
    }
    applyRoutes(this: any) {
        return applyRoutes.call(this);
    }
}