import { TrieRouter } from 'hono/router/trie-router';
import { jsxRouterConstructor, applyRoutes } from '../router';

export class JSXRouter<T> extends TrieRouter<T> {
    name: string = 'JSXRouter with TrieRouter'
    config: Record<string, any> = {}
    constructor(init: Pick<JSXRouter<T>, 'config'>) {
        super();
        jsxRouterConstructor.call(this, init);
    }
    applyRoutes(this: any) {
        return applyRoutes.call(this);
    }
}