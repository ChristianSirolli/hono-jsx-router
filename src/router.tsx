/** @jsx jsx */
/** @jsxFrag  Fragment */
import type { Context, Next } from 'hono';
import { html } from 'hono/html';
import { jsx, Fragment } from 'hono/jsx';
import { combinedRoutes } from './.combine';

declare interface JSXRouter<T> {
    name: string
    config: Record<string, any>
}

export function applyRoutes(this: any): void {
    try {
        const files: Array<string> = Object.keys(combinedRoutes);
        let Layout: any;
        if (files.includes('_layout')) {
            Layout = combinedRoutes['_layout'];
        } else {
            Layout = (props: { children?: any }) => html`<!DOCTYPE html><html><head><meta charSet="utf-8" /><meta name="viewport" content="width=device-width" /></head><body>${props.children}<body></html>`;
        }
        for (let f = 0; f < files.length; f++) {
            let Page: any = combinedRoutes[files.at(f) as string];
            let handler = async (c: Context, next: Next) => {
                // await next();
                let level = 0;
                try {
                    let layoutConfig: Record<string, any> = {
                        props: {}
                    };
                    let pageConfig: Record<string, any> = {
                        props: {}
                    };
                    level = 1;
                    if (this.config?.layout) {
                        level = 2;
                        let entries = Object.entries(this.config.layout.props);
                        level = 3;
                        for (let i = 0; i < entries.length; i++) {
                            level = 4 + (i / 100);
                            let val = entries[i][1];
                            level = 5 + (i / 100);
                            if (typeof val == 'function') {
                                level = 6 + (i / 100);
                                val = val(c);
                            }
                            level = 7 + (i / 100);
                            layoutConfig.props[entries[i][0]] = val;
                        }
                    }
                    level = 8;
                    if (this.config?.page) {
                        level = 9;
                        let entries = Object.entries(this.config.page.props);
                        level = 10;
                        for (let i = 0; i < entries.length; i++) {
                            level = 11 + (i / 100);
                            let val = entries[i][1];
                            level = 12 + (i / 100);
                            if (typeof val == 'function') {
                                level = 13 + (i / 100);
                                val = val(c);
                            }
                            level = 14 + (i / 100);
                            pageConfig.props[entries[i][0]] = val;
                        }
                    }
                    level = 15;
                    return c.html(<Layout {...layoutConfig?.props}><Page {...pageConfig?.props} params={c.req.queries()} c={c} /></Layout>);
                } catch (e) {
                    console.error(`Error occurred at level ${level}`);
                    throw e;
                }
            };
            this.add('GET', `/${(files.at(f) as string).replace('index', '')}`, handler as any);
        }
    } catch (e) {
        console.error('Found error in JSXRouter.applyRoutes', e);
        throw e;
    }
}

export function jsxRouterConstructor(this: any, init: Pick<JSXRouter<any>, 'config'>) {
    Object.assign(this, init);
}